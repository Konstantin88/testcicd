﻿using DSKitting.UI.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DSKitting.UI.Pages.Landing
{
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class LandingModel : PageModel
    {
        public void OnGet()
        {

        }
    }
}